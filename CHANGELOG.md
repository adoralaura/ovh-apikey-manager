# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
none

## [0.1.0] - 2023-06-22

### Added
- Makefile for build and clean

### Changed
- `ovh-apikey-manager delete` can now take more than one Application ID as argument. See `ovh-apikey-manager delete --help` for more info

### Removed
- `ovh-apikey-manager completion` built-in option

## [0.0.1] - 2023-06-12

### Added

- config template for `.env` config file
- Cli command to show all api keys
- Cli command to delete a single api key by KeyID

[unreleased]: https://codeberg.org/lauralani/ovh-apikey-manager/compare/v0.1.0...HEAD
[0.1.0]: https://codeberg.org/lauralani/ovh-apikey-manager/releases/tag/v0.1.0
[0.0.1]: https://codeberg.org/lauralani/ovh-apikey-manager/releases/tag/v0.0.1
