# ovh-apikey-manager
[![status-badge](https://ci.codeberg.org/api/badges/lauralani/ovh-apikey-manager/status.svg)](https://ci.codeberg.org/lauralani/ovh-apikey-manager)
[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

A tiny tool to manage your OVH api keys

## Installation
### Pre-Built Binary
To get the binary just download the latest release for your OS/Arch from the 
[release page](https://codeberg.org/lauralani/ovh-apikey-manager/releases) 
and put the binary somewhere convenient. The application assumes its config 
file to be in the same directory as the binary.

### Compile from Source
Requirements:
- go1.20+

First, clone the repository to your local machine
```shell
git clone https://codeberg.org/lauralani/ovh-apikey-manager

# or if you want a specific branch:
git clone https://codeberg.org/lauralani/ovh-apikey-manager -b x.x.x
```

Then compile the project with 
```shell
go build
```

## Configuration
The application assumes the config file `.env` in the current working directory.
If you installed from source you can just copy a template `.env` from the `examples/` directory,
or you can just [take it directly](https://codeberg.org/lauralani/ovh-apikey-manager/src/branch/main/examples/.env) from the Git repository

## Usage
You can see the list of available commands with
```bash
./ovh-apikey-manager --help
```

## Changelog
You can find the Changelog here: [Changelog](https://codeberg.org/lauralani/ovh-apikey-manager/src/branch/main/CHANGELOG.md)

## License

`ovh-apikey-manager` is available under the MIT license. See the LICENSE file for more info.
