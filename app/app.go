package app

import (
	"github.com/ovh/go-ovh/ovh"
	"github.com/spf13/viper"
	"log"
)

type ApplicationAllResponse struct {
	Applications []int
}

type Application struct {
	ApplicationID  int    `json:"applicationId"`
	ApplicationKey string `json:"applicationKey"`
	Description    string `json:"description"`
	Name           string `json:"name"`
	Status         string `json:"status"`
}

func GetOVHClient() *ovh.Client {
	if viper.GetString("OVH_APPLICATIONKEY") == "" ||
		viper.GetString("OVH_APPLICATIONSECRET") == "" ||
		viper.GetString("OVH_CONSUMERKEY") == "" ||
		viper.GetString("OVH_REGION") == "" {
		log.Fatalln("We're missing a configuration variable in .env. Please recheck the config file.")
	}
	client, _ := ovh.NewClient(
		viper.GetString("OVH_REGION"),
		viper.GetString("OVH_APPLICATIONKEY"),
		viper.GetString("OVH_APPLICATIONSECRET"),
		viper.GetString("OVH_CONSUMERKEY"),
	)

	return client
}
