/*
Copyright © 2023 Laura Kalb <dev@lauka.net>
*/
package cmd

import (
	"log"
	"os"
	"strconv"

	"codeberg.org/lauralani/ovh-apikey-manager/app"

	"github.com/spf13/cobra"
)

// deleteCmd represents the delete command
var deleteCmd = &cobra.Command{
	Use:     "delete",
	Short:   "Delete API keys",
	Long:    `Delete one or multiple api keys by ID, separated by spaces`,
	Args:    cobra.MinimumNArgs(1),
	Example: "ovh-apikey-manager delete 111111 22222 33333",
	Run:     runDelete,
}

func runDelete(cmd *cobra.Command, args []string) {
	ids, err := getIntsfromDeleteArgs(args)
	if err != nil {
		log.Printf("Arguments must be valid numbers!\n")
		os.Exit(1)
	}

	client := app.GetOVHClient()

	for _, id := range ids {
		str := strconv.Itoa(id)
		err = client.Delete("/me/api/application/"+str, nil)
		if err != nil {
			log.Printf("Error deleting application: %q\n", err)
			os.Exit(1)
		}
		log.Printf("Deleted Application %v\n", id)
	}
}

func getIntsfromDeleteArgs(args []string) ([]int, error) {
	var ints []int
	for _, arg := range args {
		num, err := strconv.Atoi(arg)
		if err == nil {
			ints = append(ints, num)
		} else {
			var empty []int
			return empty, err
		}
	}
	return ints, nil
}

func init() {
	rootCmd.AddCommand(deleteCmd)
}
