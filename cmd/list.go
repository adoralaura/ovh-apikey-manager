/*
Copyright © 2023 Laura Kalb <dev@lauka.net>
*/
package cmd

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"codeberg.org/lauralani/ovh-apikey-manager/app"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// listCmd represents the list command
var listCmd = &cobra.Command{
	Use:     "list",
	Short:   "List all active API Keys",
	Long:    `Lists all of your currently active API Keys`,
	Run:     runList,
	Args:    cobra.NoArgs,
	Example: "ovh-apikey-manager delete",
}

func runList(cmd *cobra.Command, args []string) {
	client := app.GetOVHClient()
	var applist []app.Application

	var appresponselist []int
	err := client.Get("/me/api/application", &appresponselist)
	if err != nil {
		log.Printf("Error getting application list: %q\n", err)
		os.Exit(1)
	}

	for _, appid := range appresponselist {
		var application app.Application

		err := client.Get("/me/api/application/"+strconv.Itoa(appid), &application)
		if err != nil {
			log.Printf("Error getting application %v: %v", appid, err.Error())
			os.Exit(1)
		}

		applist = append(applist, application)
	}

	fmt.Println("List of current API-Keys:")
	fmt.Println("Format: ID: [status] name (description)")
	fmt.Printf("\"*\" shows API key used by this application\n\n")

	for _, item := range applist {
		iscurrentkey := "  "
		if item.ApplicationKey == viper.GetString("OVH_APPLICATIONKEY") {
			iscurrentkey = "* "
		}
		fmt.Printf("%v%v: [%v] %v (%v)\n", iscurrentkey, item.ApplicationID, item.Status, item.Name, item.Description)
	}
}

func init() {
	rootCmd.AddCommand(listCmd)
}
