/*
Copyright © 2023 Laura Kalb <dev@lauka.net>
*/
package cmd

import (
	"log"
	"os"

	"github.com/spf13/viper"

	"github.com/spf13/cobra"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "ovh-apikey-manager",
	Short: "A tiny tool to manage your OVH api keys",
	Long: `A tiny tool to manage your OVH api keys. You can

- show your current active API keys
- delete specific API keys`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	viper.SetConfigFile(".env") // Search for a config file named .env
	viper.AddConfigPath(".")    // in the working dir of the app
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		log.Fatalln("Couldn't find config file at ./.env")
	}

	rootCmd.CompletionOptions.DisableDefaultCmd = true
	err = rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	// rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
