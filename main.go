/*
Copyright © 2023 2023 Laura Kalb <dev@lauka.net>
*/
package main

import "codeberg.org/lauralani/ovh-apikey-manager/cmd"

func main() {
	cmd.Execute()
}
